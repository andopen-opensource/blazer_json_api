# frozen_string_literal: true

BlazerJsonAPI::Engine.routes.draw do
  resources :queries, only: :show
end
