# frozen_string_literal: true

module BlazerJsonAPI
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
  end
end
