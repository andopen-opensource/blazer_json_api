# frozen_string_literal: true

module BlazerJsonAPI
  class ApplicationJob < ActiveJob::Base
  end
end
