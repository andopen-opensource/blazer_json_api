# frozen_string_literal: true

module BlazerJsonAPI
  class QueriesController < BlazerJsonAPI::ApplicationController
    before_action :set_query

    def show
      @statement = @query.statement
      data_source = @query.data_source
      process_variables(@statement, data_source)
      result = Blazer.data_sources[data_source].run_statement(@statement)

      if result.error.present?
        render_errors(Array(result.error))
      else
        render json: BlazerJsonAPI::ResultToNestedJson.new(@statement, result).call
      end
    end

    private

    def set_query
      @query = Blazer::Query.find_by(id: params[:id].to_s.split('-').first)
      record_not_found && return if @query.blank?
    end

    def process_variables(statement, data_source)
      BlazerJsonAPI::ProcessStatementVariables.new(statement, data_source, params).call
    end
  end
end
