# frozen_string_literal: true

module BlazerJsonAPI
  class ApplicationController < ActionController::Base

    if BlazerJsonAPI.credentials_provided?
      http_basic_authenticate_with name: BlazerJsonAPI.username, password: BlazerJsonAPI.password
    end

    protect_from_forgery with: :exception

    def record_not_found
      render json: [], status: :not_found
    end

    def render_errors(error_messages, status: :bad_request)
      render json: { errors: error_messages }, status: status
    end
  end
end
