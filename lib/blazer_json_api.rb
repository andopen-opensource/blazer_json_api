# frozen_string_literal: true

require 'blazer_json_api/engine'
require 'blazer_json_api/configuration'
require 'blazer_json_api/process_statement_variables'
require 'blazer_json_api/result_to_nested_json'

module BlazerJsonAPI
  extend BlazerJsonAPI::Configuration
end
