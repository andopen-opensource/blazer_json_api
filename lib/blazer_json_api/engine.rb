# frozen_string_literal: true

module BlazerJsonAPI
  class Engine < ::Rails::Engine
    isolate_namespace BlazerJsonAPI

    config.generators do |g|
      g.test_framework :rspec
    end
  end
end
