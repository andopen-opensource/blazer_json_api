# frozen_string_literal: true

# Taken from: https://github.com/ankane/blazer/blob/ac7e02e0fe19ee6c0bc8c7f3ba87be0d260f8255/app/controllers/blazer/base_controller.rb#L35
# Necessary for processing any query variables safely
# Refactored to avoid rubocop breaches
module BlazerJsonAPI
  class ProcessStatementVariables
    attr_reader :statement, :data_source, :params

    def initialize(statement, data_source, params)
      @statement = statement
      @data_source = data_source
      @params = params
    end

    def call
      bind_variables.each do |variable|
        value = params[variable].presence
        if value
          if variable.end_with?('_at')
            begin
              value = Blazer.time_zone.parse(value)
            rescue StandardError
              # do nothing
            end
          end

          if /\A\d+\z/.match?(value.to_s)
            value = value.to_i
          elsif /\A\d+\.\d+\z/.match?(value.to_s)
            value = value.to_f
          end
        end
        if Blazer.transform_variable
          value = Blazer.transform_variable.call(variable, value)
      end
        statement.gsub!("{#{variable}}", ActiveRecord::Base.connection.quote(value))
      end
    end

    private

    def bind_variables
      @bind_variables ||=
        begin
          (bind_variables ||= []).concat(Blazer.extract_vars(statement)).uniq!
          bind_variables.each do |variable|
            params[variable] ||= Blazer.data_sources[data_source].variable_defaults[variable]
          end
        end
    end
  end
end
