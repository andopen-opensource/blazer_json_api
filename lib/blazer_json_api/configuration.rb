# frozen_string_literal: true

module BlazerJsonAPI
  module Configuration

    VALID_OPTIONS_KEYS = %i[username password nesting_column_separator].freeze

    DEFAULT_USERNAME = nil
    DEFAULT_PASSWORD = nil
    DEFAULT_NESTING_COLUMN_SEPARATOR = '__'.freeze

    attr_accessor(*VALID_OPTIONS_KEYS)

    def self.extended(base)
      base.reset_config!
    end

    # Convenience method to allow configuration options to be set in a block
    # e.g. in an initializer for the application
    def configure
      yield self
    end

    def credentials_provided?
      username.present? && password.present?
    end

    def reset_config!
      self.username     = DEFAULT_USERNAME
      self.password     = DEFAULT_PASSWORD
      self.nesting_column_separator  = DEFAULT_NESTING_COLUMN_SEPARATOR
    end
  end
end
