# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'blazer_json_api/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'blazer_json_api'
  s.version     = BlazerJsonAPI::VERSION
  s.authors     = ['John Farrell']
  s.email       = ['john.farrell@andopen.co']
  s.homepage    = 'https://gitlab.com/andopen/blazer_json_api'
  s.summary     = 'An API extension to the Blazer gem'
  s.description = 'An extension to the Blazer gem that makes it possible to expose queries as APIs'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir["spec/**/*"]

  s.required_ruby_version = ">= 2.5"

  s.add_dependency 'railties', '>= 5'
  s.add_dependency 'blazer', '~> 2.0'
  s.add_development_dependency 'rspec-rails'
end
